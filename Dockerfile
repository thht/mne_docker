FROM continuumio/miniconda3

COPY environment.yml environment.yml

RUN apt update && apt install -y build-essential libgl1-mesa-glx libxt6 && \
    conda config --append channels obob && \
    conda config --append channels conda-forge && \
    conda create -n mne python && \
    conda env update -n mne && \
    conda clean -y --all

RUN rm environment.yml

ENV PATH /opt/conda/envs/mne/bin:$PATH

RUN python -c "import mne; mne.datasets.testing.data_path(force_update=True); mne.datasets.sample.data_path(force_update=True)"